/////////////////  Raphael Keele 
//////////// CSE 02
/////////// HW 4, 2/16/2019




public class PokerHandCheck {
    public static void main (String [] args ) {
        int card1, card2, card3, card4, card5;  //////////////////////// we have to generate five different cards 
        
        card1 = (int)(Math.random() * (52 -1 ) + 1);
        card2 = (int)(Math.random() * (52 -1 ) + 1); ////////////// while this will involve a lot of code, it will allow the program to be completed easier. 
        card3 = (int)(Math.random() * (52 -1 ) + 1);
        card4 = (int)(Math.random() * (52 -1 ) + 1);
        card5 = (int)(Math.random() * (52 -1 ) + 1);
        
        String identity, identity2, identity3, identity4, identity5; //////////////////// we also need five different identities
        identity = "";
        identity2 = "";
        identity3 = "";
        identity4 = "";
        identity5 = "";
        
        
        String suit, suit2, suit3, suit4, suit5; /////////////// and five diff. suites
        suit = ""; 
        suit2 = ""; 
        suit3 = ""; 
        suit4 = ""; 
        suit5 = ""; 
        
        
        
        int temp, temp1, temp2, temp3, temp4; ///////////////// and five diff. temps to the corresponding temps.
        
        temp = card1; 
        temp1 = card2;
        temp2 = card3;
        temp3 = card4;
        temp4 = card5;
        
        
        /////////////////// for card 1 
        
        if ( 1 <= card1 && card1 <= 13) {
            suit = "Diamonds"; 
        } 
        
        if (14 <= card1 && card1 <= 26) {
            suit = "Clubs";
            temp = card1 - 13;
        } 
        
        if (27 <= card1 && card1 <= 39) {
            suit = "Hearts";
            temp = card1 - 26;
        }
        
        if (40 <= card1 && card1 <= 52 ) {
            suit = "Spades"; 
            temp = card1 - 39; 
        }
        
        
        ///////////////////// for card 2
        
         if ( 1 <= card2 && card2 <= 13) { 
            suit2 = "Diamonds"; 
        } 
        
        if (14 <= card2 && card2 <= 26) {
            suit2 = "Clubs";
            temp1 = card2 - 13; 
        } 
        
        if (27 <= card2 && card2 <= 39) {
            suit2 = "Hearts";
            temp1 = card2 - 26;
        }
        
        if (40 <= card2 && card2 <= 52 ) {
            suit2 = "Spades"; 
            temp1 = card2 - 39; 
        }
        
        //////////////////// for card 3
        
         if ( 1 <= card3 && card3 <= 13) { 
            suit3 = "Diamonds"; 
        } 
        
        if (14 <= card3 && card3 <= 26) {
            suit3 = "Clubs";
            temp2 = card3 - 13; 
        } 
        
        if (27 <= card3 && card3 <= 39) {
            suit3 = "Hearts";
            temp2 = card3 - 26;
        }
        
        if (40 <= card3 && card3 <= 52 ) {
            suit3 = "Spades"; 
            temp2 = card3 - 39; 
        }
        
        
        //////////////////////// for card 4
        
         if ( 1 <= card4 && card4 <= 13) { 
            suit4 = "Diamonds"; 
        } 
        
        if (14 <= card4 && card4 <= 26) {
            suit4 = "Clubs";
            temp3 = card4 - 13; 
        } 
        
        if (27 <= card4 && card4 <= 39) {
            suit4 = "Hearts";
            temp3 = card4 - 26;
        }
        
        if (40 <= card4 && card4 <= 52 ) {
            suit4 = "Spades"; 
            temp3 = card4 - 39; 
        }
        
        
        ///////////////////// for card 5 
        
         if ( 1 <= card5 && card5 <= 13) { 
            suit5 = "Diamonds"; 
        } 
        
        if (14 <= card5 && card5 <= 26) {
            suit5 = "Clubs";
            temp4 = card5 - 13; 
        } 
        
        if (27 <= card5 && card5 <= 39) {
            suit5 = "Hearts";
            temp4 = card5 - 26;
        }
        
        if (40 <= card5 && card5 <= 52 ) {
            suit5 = "Spades"; 
            temp4 = card5 - 39; 
        }

     
      
        ////////////////////// switch statements for cards 1 - 5
        
        /////////////// temp:
        
         switch (temp) {  ///////////  this the new card variable, so all card variables are between 1 and 13 now
            case 1: identity = "Ace"; //////////// in replace of one
            break; 
            case 2: identity = "2";  
            break; ////// stops fallout
            case 3: identity = "3"; 
            break; 
            case 4: identity = "4";
            break; 
            case 5: identity = "5";
            break; 
            case 6: identity = "6";
            break; 
            case 7: identity = "7"; 
            break;
            case 8: identity = "8"; 
            break; 
            case 9: identity = "9"; 
            break; 
            case 10: identity= "10"; 
            break; 
            case 11: identity = "Jack";  //////// there cannot be an 11, so it has to be jack
            break;
            case 12: identity = "Queen"; ////// same logic as before
            break; 
            case 13: identity = "King";
            break; 
        }
        
        //////////// temp2
    
         switch (temp1) {  ///////////  this the new card variable, so all card variables are between 1 and 13 now
            case 1: identity2 = "Ace"; //////////// in replace of one
            break; 
            case 2: identity2 = "2";  
            break; ////// stops fallout
            case 3: identity2 = "3"; 
            break; 
            case 4: identity2 = "4";
            break; 
            case 5: identity2 = "5";
            break; 
            case 6: identity2 = "6";
            break; 
            case 7: identity2 = "7"; 
            break;
            case 8: identity2 = "8"; 
            break; 
            case 9: identity2 = "9"; 
            break; 
            case 10: identity2 = "10"; 
            break; 
            case 11: identity2 = "Jack";  //////// there cannot be an 11, so it has to be jack
            break;
            case 12: identity2 = "Queen"; ////// same logic as before
            break; 
            case 13: identity2 = "King";
            break; 
        }
        
        ////////////////// temp3 
        
         switch (temp2) {  ///////////  this the new card variable, so all card variables are between 1 and 13 now
            case 1: identity3 = "Ace"; //////////// in replace of one
            break; 
            case 2: identity3 = "2";  
            break; ////// stops fallout
            case 3: identity3 = "3"; 
            break; 
            case 4: identity3 = "4";
            break; 
            case 5: identity3 = "5";
            break; 
            case 6: identity3 = "6";
            break; 
            case 7: identity3 = "7"; 
            break;
            case 8: identity3 = "8"; 
            break; 
            case 9: identity3 = "9"; 
            break; 
            case 10: identity3 = "10"; 
            break; 
            case 11: identity3 = "Jack";  //////// there cannot be an 11, so it has to be jack
            break;
            case 12: identity3 = "Queen"; ////// same logic as before
            break; 
            case 13: identity3 = "King";
            break; 
        }
        
        //////////////////// temp 4
        
         switch (temp3) {  ///////////  this the new card variable, so all card variables are between 1 and 13 now
            case 1: identity4 = "Ace"; //////////// in replace of one
            break; 
            case 2: identity4 = "2";  
            break; ////// stops fallout
            case 3: identity4 = "3"; 
            break; 
            case 4: identity4 = "4";
            break; 
            case 5: identity4 = "5";
            break; 
            case 6: identity4 = "6";
            break; 
            case 7: identity4 = "7"; 
            break;
            case 8: identity4 = "8"; 
            break; 
            case 9: identity4 = "9"; 
            break; 
            case 10: identity4= "10"; 
            break; 
            case 11: identity4 = "Jack";  //////// there cannot be an 11, so it has to be jack
            break;
            case 12: identity4 = "Queen"; ////// same logic as before
            break; 
            case 13: identity4 = "King";
            break; 
        }
        
        //////////////////
      
        
        /////////////// temp 5 
        
         switch (temp4) {  ///////////  this the new card variable, so all card variables are between 1 and 13 now
            case 1: identity5 = "Ace"; //////////// in replace of one
            break; 
            case 2: identity5 = "2";  
            break; ////// stops fallout
            case 3: identity5 = "3"; 
            break; 
            case 4: identity5 = "4";
            break; 
            case 5: identity5 = "5";
            break; 
            case 6: identity5 = "6";
            break; 
            case 7: identity5 = "7"; 
            break;
            case 8: identity5 = "8"; 
            break; 
            case 9: identity5 = "9"; 
            break; 
            case 10: identity5 = "10"; 
            break; 
            case 11: identity5 = "Jack";  //////// there cannot be an 11, so it has to be jack
            break;
            case 12: identity5 = "Queen"; ////// same logic as before
            break; 
            case 13: identity5 = "King";
            break; 
        }

            
            
            /////////// print statements for everything 
            
            System.out.println("Your random cards are: ");
            System.out.println("");
            
            
            System.out.println("the " + identity + " of " + suit);
            System.out.println("the " + identity2 + " of " + suit2);
            System.out.println("the " + identity3 + " of " + suit3);
            System.out.println("the " + identity4 + " of " + suit4);
            System.out.println("the " + identity5 + " of " + suit5);
            
            System.out.println("");
      
      
             ////////////////////// boolean true/false
      
             /// 3 booleans: a, b, c
             //// 
              boolean a,b,c;
              
              a = false; //// the basic one
              b = false; //// varible for pair 
              c = false; ////// variable for three of a kind 
      

            ////////////////////// all the conditions needed for these to be three of a kind 
      
             if (identity == identity2 && identity == identity3) { 
                c = true; 
            } else if (identity == identity2 && identity == identity4) {
                c = true;
            } else if (identity == identity2 && identity == identity5) {
                c = true;
            } else if (identity == identity3 && identity == identity4) {
                c = true;
            } else if (identity == identity3 && identity == identity5) {
                c = true;
            } else if (identity == identity4 && identity == identity5) {
                c = true;
            } else if (identity2 == identity3 && identity2 == identity4) {
                c = true;
            } else if (identity2 == identity3 && identity2 == identity5) {
                c = true;
            } else if (identity2 == identity4 && identity2 == identity5) {
                c = true;
            } else if (identity3 == identity4 && identity3 == identity5) {
                c = true;
            } 
      
             ///////////////////// all the conditions needed for pairs
            if (identity == identity2 || identity == identity3 || identity == identity4) {
                b = true;
            } else if (identity2 == identity3 || identity2 == identity4) {
                 b = true;                
            } else if (identity3 == identity4) {
                 b = true;
            } else if (identity5 == identity || identity5 == identity2 || identity5 == identity3 || identity5 == identity4) {
                 b = true;
            }
      
            //////// boolean comparisons
      
            if (c == true) {
              System.out.println("You have three of a kind!"); 
            } else if (b == true) {
              System.out.println("You have a pair!"); 
            } else if (c != true || b!= true) {
              a = false;
              System.out.println("You have a high card hand.");
            }
      
      
             
      
      
    }
}

