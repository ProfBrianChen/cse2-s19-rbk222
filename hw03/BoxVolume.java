//////// CSE 2, HW 3 
//////// Raphael Keele 
/////// 849241448 2/8/19


import java.util.Scanner; // the scanner import that is nessecary for programs
public class BoxVolume { // class name and file name 
    public static void main (String args []) { // main method 
        Scanner input = new Scanner(System.in); /// Scanner method
        double width, length, height, volume; /// defining the methods 
        System.out.println("The width side of the box is: "); /// instructing the user to enter the width
        width = input.nextDouble(); //////// input variable for width 
        System.out.println("The length of the box is: "); ///// instructing the user to enter the length 
        length = input.nextDouble(); ///// input variable for length
        System.out.println("The height of the box is: "); ///// instructing the user to enter the height
        height = input.nextDouble(); ///// input variable for height
        volume = length * width * height;  /////// formula for box volume  
        System.out.println("The volume inside of the box is: " + volume);  
    }
    
}
