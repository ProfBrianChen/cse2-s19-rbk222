//////// CSE 2, HW 3 
//////// Raphael Keele 
/////// 849241448 2/8/19


import java.util.Scanner; // the scanner import that is necessary for programs
public class Convert { // class name and file name 
    public static void main (String args []) { // main method 
        Scanner input = new Scanner(System.in); /// Scanner method
        double meters, inches;    //stores the values in 
        System.out.println("Enter the distance in meters: "); // directs the user to enter in certain things 
        meters = input.nextDouble(); // stores the meters value 
        inches = meters * 39.37;  // formula for meters to inches 
        System.out.println(meters + " meters is " + inches + " inches");
    }
    
}
