import java.util.*;

public class Straight {
    
    public static int[] shuffle(int [] x) {
        
        Random rgen = new Random();  		
 
		for (int i=0; i<x.length; i++) {
		    int randomPosition = rgen.nextInt(x.length);
		    int temp = x[i];
		    x[i] = x[randomPosition];
		    x[randomPosition] = temp;
		}
        
        
        return x;
    }
    
    
    public static int [] draw(int [] x) {
        
        
        int [] hand = new int [5]; 
        
        for(int i = 0; i < 5; i++) {
            
            hand[i] = x[i];
            
        }
        return hand;
    }
    
    
    public static boolean check(int []x, int k){
        
            for (int i = 1; i < x.length; i++) {
                if (x[i] != x[i - 1] + 1){
                        return false;
                    } else if (k < 0 && k > 5) {
                        System.out.print("ERROR");
                        return false;
                    }
                }
            return true;
        
        
    }
    
    
    public static void main(String []args) {
        
        
        int [] deck = new int [52]; 
        
        for (int i = 0; i < 52; i++) {
            deck[i] = i + 1; 
          //System.out.print(" " + deck[i] + " ");
        }//System.out.println();
        
        
        int [] shuffled = shuffle(deck);
        
        System.out.println("Shuffled deck");
        
        for (int i = 0; i < 52; i++) {
            System.out.print(" " + deck[i] + " ");
        }System.out.println();
        
        int [] hand = draw(shuffled); 
        
        
        System.out.println("Hand");
        
        for (int i = 0; i < 5; i++) {
          System.out.print(" " + deck[i] + " ");
        }System.out.println();
        
        
        int k = (int) (Math.random()*52 + 1);
        boolean straight = check(hand, k);
        
        if (straight == true) {
            System.out.println(" This is a straight.");
            
        } else if(straight == false)  {
            System.out.println("This is not a straight.");
        }
        
    }
     
    
    
}