import java.util.Scanner; 

public class booleans2{
  public static void main (String args []) {
    String smiling, smiling2; 
    boolean aSmile, bSmile, monkeytrouble1, monkeytrouble2, monkeytrouble3; 
    
    aSmile = true; 
    bSmile = false; 
    
    monkeytrouble1 = !aSmile && !bSmile; 
    monkeytrouble2 = aSmile && bSmile; 
    monkeytrouble3 = false;
    
    Scanner input = new Scanner (System.in); 
    
    System.out.println("Are both the monkeys smiling? Enter yes or no");
    smiling = input.next();
    
    System.out.println("Are neither of the monkeys smiling? Enter yes or no");
    smiling2 = input.next(); 
    
    if (smiling.equals("yes") && smiling2.equals("yes")) {
      System.out.println(monkeytrouble1 + " We are in trouble.");
    } 
    
    if (smiling.equals("no") && smiling2.equals("no")){
      System.out.println(monkeytrouble2 + " We are in trouble.");
    }
    
    if (smiling.equals("yes") && (smiling2.equals("no") )) {
      System.out.println(monkeytrouble3 + " We are not in trouble"); 
    }
    
    if (!smiling.equals("yes") && !smiling.equals("no") && !smiling2.equals("no") && !smiling2.equals("no")){
      System.out.println("ERROR: YOU DID NOT ANSWER PROPERLY, PLEASE ANSWER AGAIN");
    }
  
  
  }
}