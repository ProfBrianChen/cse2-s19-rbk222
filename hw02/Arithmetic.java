///// Raphael Keele 2/1/2019
//// CSE 2, HW 2 
/// 849241448 

public class Arithmetic {
    public static void main (String args []) { 
        int numPants, numShirts, numBelts; /// these variables are used to store the values of each shirt, pants and belts bought 
        numPants = 3; // number of pants 
        numShirts = 2; // number of shirts 
        numBelts = 1; // number of belts 
    
        double pantsPrice, shirtPrice, beltCost; /// these are the variables that are used to store the cost of each pants, shirt and belt
        pantsPrice = 34.98; // the costs of each pair of pants    
        shirtPrice = 24.99; // the costs of each shirts 
        beltCost = 33.99; // the costs of each belt 
    
        double costofPants, costofShirts, costofBelts; /// these variables store the total costs of pants, shirts, and belts 
        costofPants = pantsPrice * numPants; // the total cost of pants 
        costofShirts = numShirts * shirtPrice; // the total cost of shirts 
        costofBelts = numBelts * beltCost; // the total cost of each belt 
    
        double paSalesTax = 0.06; // the sales tax per dollar spent 
        
        double pantsTax, shirtsTax, beltsTax; // the variables that store the tax 
        pantsTax = (costofPants * paSalesTax * 100) ; // tax for pants
        shirtsTax = (costofShirts * paSalesTax * 100) ; // tax for shirts 
        beltsTax = (costofBelts * paSalesTax * 100); // tax for belts 
        
         
        int pantsTax1 = (int) pantsTax ;   /// for truncation purposes, the variables must be turned back to ints
        int shirtsTax1 = (int) shirtsTax ; 
        int beltsTax1 = (int) beltsTax ; 
        
        double pantsTax2 = (double) pantsTax1/100; /// by converting back to doubles, you can make it easier
        double shirtsTax2 = (double) shirtsTax1/100; // to print values to the hundreth decimal place 
        double beltsTax2 = (double) beltsTax1/100; /// like what a regular dollar and cents value is 
    
        double Subtotal, TaxTotal, Total; // the totals of everything 
        Subtotal = costofPants + costofShirts + costofBelts;  
        TaxTotal = (pantsTax2 + shirtsTax2 + beltsTax2); 
        Total = (Subtotal + TaxTotal); 
        
        
        System.out.println("The total costs of pants is: $ "+ costofPants); 
        System.out.println("The total tax for pants is: $ "+ pantsTax2); 
        System.out.println("The total costs of shirts is: $ "+ costofShirts); 
        System.out.println("The total tax for shirts is: $ "+ shirtsTax2); 
        System.out.println("The total cost for belts is: $ "+ costofBelts); 
        System.out.println("The total tax for belts is: $ "+ beltsTax2); 
        
        System.out.println("Subtotal: "+ Subtotal); 
        System.out.println("Tax Total: "+ TaxTotal); 
        System.out.println("Total: "+ Total); 
    
    }
}

